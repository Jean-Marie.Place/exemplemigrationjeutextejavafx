package mouton;

import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class GuiInterface extends Application {
	private final int NB_CASES = 9;
	private Stage stage;
	private JeuMouton modele = new JeuMouton(NB_CASES);
	private Pane pane;
	private Rectangle[] rectangles = new Rectangle[NB_CASES];
	private Circle[] circles = new Circle[NB_CASES];

	public void start(Stage stage) {
		this.stage = stage;
		VBox root = new VBox();

		root.setSpacing(5.0);

		// Zone d'affichage du jeu
		pane = new Pane();
		pane.setPrefWidth(400.0);
		pane.setPrefHeight(400.0);

		Button btnQuit = new Button("Quitter");
		btnQuit.setPrefWidth(Double.MAX_VALUE);
		btnQuit.setOnAction(e -> { stage.close(); });


		Button btnRestart = new Button("Restart");
		btnRestart.setPrefWidth(Double.MAX_VALUE);
		btnRestart.setOnAction(e -> { 
			modele.renew(); 
			updateGame() ;
		});

		root.getChildren().addAll(pane, btnQuit, btnRestart);

		for (int i = 0; i < NB_CASES; i++) {
			rectangles[i] = new Rectangle();
			rectangles[i].setFill(Color.LIGHTBLUE);
			rectangles[i].setStroke(Color.BLACK);

			circles[i] = new Circle();
			circles[i].setStroke(Color.BLACK);
		}

		pane.getChildren().addAll(rectangles);
		pane.getChildren().addAll(circles);

		Scene scene = new Scene(root, 400, 300);
		stage.setScene(scene);
		stage.setTitle("Dessine-moi un mouton");
		stage.show();

		updateSize();
		updateGame();

		// Surveille la taille du pane 
		pane.heightProperty().addListener(e -> updateSize()); 
		pane.widthProperty().addListener(e -> updateSize());

		// Surveille le clic souris
		for (int i = 0; i < NB_CASES; i++) {
			int position = i;
			circles[i].setOnMouseClicked(e -> {
				play(position);
			});
			circles[i].setOnDragDetected(e -> {
				if (modele.coupsPossibles().contains(position)) {
			        Dragboard db = circles[position].startDragAndDrop(TransferMode.MOVE);
			        ClipboardContent content = new ClipboardContent();
			        content.putString(Integer.toString(position));
			        db.setContent(content);
			        e.consume();
				}
			});
			rectangles[i].setOnDragOver(e -> {
				if (modele.idxVide == position) {
					e.acceptTransferModes(TransferMode.MOVE);
					e.consume();
				}
			});
			rectangles[i].setOnDragDropped(e -> {
				Dragboard db = e.getDragboard();
				if (db.hasString()) {			
					int srce = Integer.parseInt(db.getString());
					play(srce);
					e.setDropCompleted(true);
					e.consume();
				} else {
					e.setDropCompleted(false);
					e.consume();
				}
			});
		}
	}

	private void alertWin() {
		Alert alert = new Alert(AlertType.INFORMATION);  
		alert.setTitle("Résultat");
		alert.setHeaderText("Fin du jeu");
		alert.setContentText("Bravo, vous avez gagné !");
		alert.showAndWait();
	}

	private void alertLoose() {
		Alert alert = new Alert(AlertType.INFORMATION);  
		alert.setTitle("Résultat");
		alert.setHeaderText("Fin du jeu");
		alert.setContentText("Désolé, plus aucun coup disponible. Vous avez perdu.");
		alert.showAndWait();
	}

	private void play(int coup) {
		if (modele.coupsPossibles().contains(coup)) {
			modele.executer(coup);
			updateGame();
		} else {
			System.out.println("interdit");
		}
	}

	/**
	 * Recalcule la taille des éléments graphiques à partir de la taille du pane contenant
	 */
	private void updateSize() {
		double caseSize;
		double caseBaseX;
		double caseBaseY;
		caseSize = pane.getWidth() / (NB_CASES + 2);
		caseBaseY = ((pane.getHeight() - caseSize) / 2);
		caseBaseX = caseSize;
		for (int i = 0; i < NB_CASES; i++) {
			rectangles[i].setHeight(caseSize);
			rectangles[i].setWidth(caseSize);
			rectangles[i].setX(caseBaseX + i * caseSize);
			rectangles[i].setY(caseBaseY);
			circles[i].setCenterX(caseBaseX + caseSize * (i + 0.5));
			circles[i].setCenterY(caseBaseY + caseSize / 2);
			circles[i].setRadius(caseSize * 0.4);
		}
	}

	/**
	 * Met à jour l'affichage en fonction de l'état du jeu (modele).
	 * Note la position des pions ne change pas ! l ancien pion deviens invisible et 
	 * l'ancien pion invisible prend la nouvelle couleur 
	 */
	private void updateGame() {
		for (int i = 0; i < NB_CASES; i++) {
			switch (modele.jeu[i]) {
			case BLANC:
				circles[i].setVisible(true);
				circles[i].setStroke(Color.BLACK);
				circles[i].setFill(Color.WHITE);
				break;
			case NOIR:
				circles[i].setVisible(true);
				circles[i].setStroke(Color.BLACK);
				circles[i].setFill(Color.BLACK);
				break;
			case VIDE:
				circles[i].setVisible(false);
				break;
			}
		}
		if (modele.gagnant()) {
			alertWin();
			stage.close();
		} else if (modele.perdant()) {
			alertLoose();
			stage.close();
		}
	}

	public static void main(String[] args) {
		Application.launch(args);
	}
}