package mouton;

import java.util.Set;
import java.util.TreeSet;

public class JeuMouton implements Jeu {
	
	public enum Case { BLANC('>'), NOIR('<'), VIDE(' ') ; 
		private char visu ;

		private Case(char visu) {
			this.visu = visu;
		}
		public String toString() {
			return  "" + visu ;
		}
	} ;
	
	public final int SIZE ;
	protected int idxVide ;
	protected Case[] jeu = null ;
	
	public JeuMouton(int size) {
		SIZE = size ;
		jeu = new Case[SIZE] ;
		renew() ;
	}
	
	@Override
	public Set<Integer> coupsPossibles() {
		Set<Integer> result = new TreeSet<Integer>() ;
		for (int i = 0 ; i < SIZE ; i++) {
			if (ok(i)) result.add(i) ;
		}
		return result ;
	}
	
	/**
	 * Détermine si un coup (définit par l'indice du mouton qui bouge) est correct.
	 * @param c le coup testé
	 * @return true si le coup est licite.
	 */
	protected boolean ok(int c) {
		if ((c-2 == idxVide) && (jeu[c] == Case.NOIR)) return true ;
		if ((c-1 == idxVide) && (jeu[c] == Case.NOIR)) return true ;
		if ((c+1 == idxVide) && (jeu[c] == Case.BLANC)) return true ;
		if ((c+2 == idxVide) && (jeu[c] == Case.BLANC)) return true ;
		return false ;
	}
	
	@Override
	public void executer(int coup) {
            if ((coup-2 == idxVide) && (jeu[coup] == Case.NOIR)) { 
            	jeu[idxVide] = jeu[coup] ; jeu[coup] = Case.VIDE ; idxVide = coup ; 
        	} else if ((coup-1 == idxVide) && (jeu[coup] == Case.NOIR)) { 
            	jeu[idxVide] = jeu[coup] ; jeu[coup] = Case.VIDE ; idxVide = coup ; 
        	} else if ((coup+1 == idxVide) && (jeu[coup] == Case.BLANC)) { 
            	jeu[idxVide] = jeu[coup] ; jeu[coup] = Case.VIDE ; idxVide = coup ;
        	} else if ((coup+2 == idxVide) && (jeu[coup] == Case.BLANC)) { 
            	jeu[idxVide] = jeu[coup] ; jeu[coup] = Case.VIDE ; idxVide = coup ; 
        	} 
	}
	
	@Override
	public void renew() {
		idxVide = (SIZE / 2) ;
		for (int i = 0 ; i < idxVide ; i++) jeu[i] = Case.BLANC ;
		for (int i = SIZE-1 ; i > idxVide ; i--) jeu[i] = Case.NOIR ;
		jeu[idxVide] = Case.VIDE ;
	}

	@Override
	public boolean gagnant() {
		for (int i = 0 ; i < SIZE; i++) {
			if ((i > (SIZE /2)) && (jeu[i] != Case.BLANC)) return false ;
			if ((i < (SIZE /2)) && (jeu[i] != Case.NOIR)) return false ;
			if ((i == (SIZE /2)) && (jeu[i] != Case.VIDE)) return false ;
		}
		return true ;
	}
	
	@Override
	public boolean perdant() {
		return (coupsPossibles().isEmpty() && (! gagnant())) ;
	}
	
	public String toString() {
		String result = "" ;
		for (int i = 0 ; i < SIZE ; i++) {
			result += this.jeu[i] ;
		}
		return result ;
	}
}
