package mouton;

import java.util.Scanner;
import java.util.Set;

/**
 * Classe principale pour jeu en mode texte
 * @author jmpla
 *
 */
public class MainLoop {
	protected final static String CMD_NEW = "n" ;
	protected final static String CMD_HELP = "h" ;
	protected final static String CMD_QUIT = "q" ;

	protected Scanner scanner = new Scanner(System.in); 
	protected Jeu jeu = new JeuMouton(9) ;
	protected boolean continuer = true ;
	
	/**
	 * réinitialisation d'une partie
	 */
	protected void do_new() {
		jeu.renew() ;
		System.out.println("Redémarrage..") ;
	}
	
	/**
	 * Exécution d'un coup
	 * @param coup Le déplacement à exécuter (indice du mouton à déplacer).
	 */
	protected void do_play(int coup) {
		jeu.executer(coup) ;
	}
	
	/**
	 * Affichage de l'aide
	 */
	protected void do_help() {
		System.out.println("#: Tout chiffre (autorisé) pour jouer un coup") ;
		System.out.println("h (help): Cette aide.") ;
		System.out.println("n (new) : Recommencer au début") ;
	}
	
	/**
	 * Quitter le programme.
	 */
	protected void do_quit() {
		System.out.println(" Au revoir !!") ;
		continuer = false ;
	}

	/**
	 * Afficher l'état du jeu
	 * @param jeu Le jeu à afficher.
	 * @param possibles La liste des coups possibles.
	 */
	public void afficher(String jeu, Set<Integer> possibles) {
		System.out.print("     ") ;
		for (int i = 0 ; i < jeu.length() ; i++) {
			System.out.print(jeu.charAt(i)+".") ;
		}
		System.out.println();
		System.out.print("     ") ;
		for (int i = 0 ; i < jeu.length() ; i++) {
			if (possibles.contains(i)) {
				System.out.print(i+" ") ;
			} else {
				System.out.print("- ") ;
			}
		}
		System.out.println();
	}
	/**
	 * Boucle principale du programme
	 */
	protected void run() {
		jeu.renew() ;
		while (continuer) {
			Set<Integer> possibles = jeu.coupsPossibles() ;
			afficher(jeu.toString(), possibles) ;

			if (jeu.gagnant()) {
				System.out.println("Bravo !") ;
				continuer = false; } 
			else if (jeu.perdant()) {
				System.out.println("Vous avez perdu.") ;
				continuer = false; }
			else {
				System.out.print("Votre coup (") ;
				for (int c : possibles) {
					System.out.print(" " + c) ;
				}
				System.out.print (")?") ;
				System.out.println();

				if (scanner.hasNextInt()) {
					int coup = scanner.nextInt();
					System.out.println(" Coup : " + coup) ;
					if (possibles.contains(coup)) {
						do_play(coup) ;
					} else {
						System.out.println("Coup non autorisé") ;
					}
				} else {
					String commande = scanner.next() ;
					if (commande.equals(CMD_NEW)) {
						do_new() ;
					} else if (commande.equals(CMD_HELP)) {
						do_help() ;
					} else if (commande.equals(CMD_QUIT)) { 
						do_quit() ;
					} else {
						System.out.println("Commande non reconnue") ;
					}
				}
			}
		}
		System.out.println("Fin du jeu") ;
	}
	/**
	 * Lancement d'une partie.
	 * @param args unused
	 */
	public static void main(String[] args) {
		MainLoop self = new MainLoop() ;
		self.run();
	}
}

// Solution:
// 3 5 6 4 2 1 3 5 7 8 6 4 2 0 1 3 5 7 6 4 2 3 5 4