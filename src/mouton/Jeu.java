package mouton;

import java.util.Set ;
/**
 * Moteur du jeu.
 * Cette interface décrit la classe qui gére l'état du jeu.
 * Un coup est modélisé par l'entier qui repére le mouton qui se déplace.
 * 
 * @author place
 *
 */
public interface Jeu {
	/**
	 * Calcule l'ensemble des coups jouables à partir de l'état courant du jeu.
	 * @return Les coups jouables.
	 */
	Set<Integer> coupsPossibles() ;
	/**
	 * Cette méthode applique l'exécution d'un coup sur l'état courant.
	 * Si le coup proposé n'est pas autorisé, cette méthode ne fait rien.
	 * @param coup L'indice du mouton qui sera déplacé.
	 */
	public void executer(int coup) ;
	/**
	 * Réinitialise le jeu à l'état initial.
	 */
	public void renew() ;
	/**
	 * Détermine si l'état actuel du jeu est une position gagnante ("&lt;&lt;&lt;&lt; &gt;&gt;&gt;&gt;")
	 * @return true si l'état courant est gagnant.
	 */
	public boolean gagnant() ;
	/**
	 * Détermine si l'état actuel du jeu est perdant.
	 * Un état est perdant si l'état n'est pas gagnant et qu'aucun coup n'est autorisé dans cette configuration.
	 * @return true si l'état actuel est la position gagnante.
	 */
	public boolean perdant() ;
	
}
