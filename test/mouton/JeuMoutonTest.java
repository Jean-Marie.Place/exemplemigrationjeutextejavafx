package mouton;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.TreeSet;

import org.junit.Test;

public class JeuMoutonTest {

	// Methode locale utilitaire qui enchaine une sequence de déplacements
	private void executer(JeuMouton jeu, int[] coups) {
		for (int c : coups) {
			jeu.executer(c) ;
		}
	}
	
	// Q1.2 : Test du constructeur et de toString. 
	// On ne teste pas les valeurs abérantes (paires ou < 3).
	
	@Test(timeout=300)
	public void createSize9() {
		JeuMouton jeu = new JeuMouton(9) ;
		assertEquals(">>>> <<<<", jeu.toString()) ;
	}

	@Test(timeout=300)
	public void createSize13() {
		JeuMouton jeu = new JeuMouton(13) ;
		assertEquals(">>>>>> <<<<<<", jeu.toString()) ;
	}

	// Q1.3 Test de l'exécution correcte des coups.
	// On ne teste pas les déplacements illicites.
	@Test(timeout=300)
	public void initialMove3() {
		JeuMouton jeu = new JeuMouton(9) ;
		jeu.executer(3);
		assertEquals(">>> ><<<<", jeu.toString()) ;
	}
	
	@Test(timeout=300)
	public void initialMove5() {
		JeuMouton jeu = new JeuMouton(9) ;
		jeu.executer(5);
		assertEquals(">>>>< <<<", jeu.toString()) ;
	}
	
	// Le saut d'un mouton (droitier ou gaucher) au dessus d'un mouton de même type est considéré illicite.
	@Test(timeout=300)
	public void right_may_jumps_over_right() {
		JeuMouton jeu = new JeuMouton(9) ;
		jeu.executer(2);
		assertEquals(">> >><<<<", jeu.toString()) ;
	}
	
	@Test(timeout=300)
	public void left_may_jumps_over_left() {
		JeuMouton jeu = new JeuMouton(9) ;
		jeu.executer(6);
		assertEquals(">>>><< <<", jeu.toString()) ;
	}
	
	@Test(timeout=300)
	public void Move35_left_jumps_over_right() {
		JeuMouton jeu = new JeuMouton(9) ;
		executer(jeu, new int[] { 3, 5 });
		assertEquals(">>><> <<<", jeu.toString()) ;
	}
	
	@Test(timeout=300)
	public void Move35_right_jumps_over_left() {
		JeuMouton jeu = new JeuMouton(9) ;
		executer(jeu, new int[] { 5, 3 });
		assertEquals(">>> <><<<", jeu.toString()) ;
	}
	
	@Test(timeout=300)
	public void Move3546783_miscellaneous() {
		JeuMouton jeu = new JeuMouton(9) ;
		executer(jeu, new int[] { 3, 5, 4, 6, 7, 8, 3 });
		assertEquals(">>><<><< ", jeu.toString()) ;
	}
	
	// Q1.4 Test du calcul de position gagnante.
	
	@Test(timeout=300)
	public void moves_should_wins() {
		JeuMouton jeu = new JeuMouton(9) ;
		executer(jeu, 
				new int[] {  3, 5, 6, 4, 2, 1, 3, 5, 7, 8, 6, 4, 2, 0, 1, 3, 5, 7, 6, 4, 2, 3, 5, 4 }) ;
		assertEquals(true, jeu.gagnant()) ;
	}
	
	@Test(timeout=300)
	public void initial_should_not_win() {
		JeuMouton jeu = new JeuMouton(9) ;
		assertEquals(false, jeu.gagnant()) ;
	}		
	
	// Q1.5: CoupsPossibles
	@Test(timeout=300)
	public void coupsPossiblesInitial() {
		JeuMouton jeu = new JeuMouton(9) ;
		TreeSet<Integer> expected = new TreeSet<>(Arrays.asList(new Integer[] { 2, 3, 5, 6} )) ;
		assertEquals(expected, jeu.coupsPossibles()) ;
	}
	
	@Test(timeout=300)
	public void coupsPossibles_after_3() {
		JeuMouton jeu = new JeuMouton(9) ;
		executer(jeu, new int[] { 3 }) ;
		TreeSet<Integer> expected = new TreeSet<>(Arrays.asList(new Integer[] { 1, 2, 5 } )) ;
		assertEquals(expected, jeu.coupsPossibles()) ;
	}
	
	@Test(timeout=300)
	public void coupsPossibles_after_5() {
		JeuMouton jeu = new JeuMouton(9) ;
		executer(jeu, new int[] { 5 }) ;
		TreeSet<Integer> expected = new TreeSet<>(Arrays.asList(new Integer[] { 3, 6, 7 } )) ;
		assertEquals(expected, jeu.coupsPossibles()) ;
	}
	
	@Test(timeout=300)
	public void coupsPossibles_after_left_reaching_right_side() {
		JeuMouton jeu = new JeuMouton(9) ;
		executer(jeu, new int[] {  3, 5, 6, 4, 5, 7, 8, 6 }) ;
		TreeSet<Integer> expected = new TreeSet<>(Arrays.asList(new Integer[] { 7 } )) ;
		assertEquals(expected, jeu.coupsPossibles()) ;
	}
	
	@Test(timeout=300)
	public void coupsPossibles_before_left_reaching_right_side() {
		JeuMouton jeu = new JeuMouton(9) ;
		executer(jeu, new int[] {  3, 5, 6, 4, 5, 7, 8 }) ;
		TreeSet<Integer> expected = new TreeSet<>(Arrays.asList(new Integer[] { 6 } )) ;
		assertEquals(expected, jeu.coupsPossibles()) ;
	}
	
	@Test(timeout=300)
	public void coupsPossibles_after_right_reaching_left_side() {
		JeuMouton jeu = new JeuMouton(9) ;
		executer(jeu, new int[] {  5, 3, 4, 2, 1, 3, 2, 0, 1 }) ;
		TreeSet<Integer> expected = new TreeSet<>(Arrays.asList(new Integer[] {  } )) ;
		assertEquals(expected, jeu.coupsPossibles()) ;
	}
	
	@Test(timeout=300)
	public void coupsPossibles_before_right_reaching_left_side() {
		JeuMouton jeu = new JeuMouton(9) ;
		executer(jeu, new int[] {  5, 3, 4, 2, 1, 3, 2, 0 }) ;
		TreeSet<Integer> expected = new TreeSet<>(Arrays.asList(new Integer[] { 1 } )) ;
		assertEquals(expected, jeu.coupsPossibles()) ;
	}

	// Q1.6: Test position perdante
	@Test(timeout=300)
	public void moves_3210_should_loose() {
		JeuMouton jeu = new JeuMouton(9) ;
		executer(jeu, 
				new int[] {  3, 2, 1, 0 }) ;
		assertEquals(true, jeu.perdant()) ;
	}
	
	@Test(timeout=300)
	public void moves_5678_should_loose() {
		JeuMouton jeu = new JeuMouton(9) ;
		executer(jeu, 
				new int[] {  3, 2, 1, 0 }) ;
		assertEquals(true, jeu.perdant()) ;
	}
	
	@Test(timeout=300)
	public void moves_354657867_should_loose() {
		JeuMouton jeu = new JeuMouton(9) ;
		executer(jeu, 
				new int[] {  3, 5, 4, 6, 5, 7, 8, 6, 7 }) ;
		assertEquals(true, jeu.perdant()) ;
	}

	@Test(timeout=300)
	public void initial_should_not_loose() {
		JeuMouton jeu = new JeuMouton(9) ;
		assertEquals(false, jeu.perdant()) ;
	}
}
